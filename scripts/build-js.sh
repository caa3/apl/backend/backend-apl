#!/bin/bash -e

if [[ -n "${CI_COMMIT_TAG}" ]]; then
  VERSION=$(echo $CI_COMMIT_TAG)
else
  VERSION="0.0.0-SNAPSHOT"
fi
echo "Version: ${VERSION}"

# yarn install
mv /app/node_modules "${CI_PROJECT_DIR}/node_modules"
yarn check --integrity
yarn check --verify-tree
yarn build

#!/bin/bash -e

if [[ -n "${CI_COMMIT_TAG}" ]]; then
  VERSION=$(echo $CI_COMMIT_TAG)
else
  VERSION="0.0.0-SNAPSHOT"
fi
echo "Version: ${VERSION}"

IMAGE_NAME=$(echo ${CI_PROJECT_NAME} | sed -e "s@-apl@@g")
docker login -u ${CAA3_DOCKERHUB_USER} -p ${CAA3_DOCKERHUB_PASSWORD}
docker build -f "${CI_PROJECT_DIR}/docker/Dockerfile" -t "${CAA3_DOCKERHUB_USER}/${IMAGE_NAME}:${VERSION}" ${CI_PROJECT_DIR}
docker push ${CAA3_DOCKERHUB_USER}/${IMAGE_NAME}:${VERSION}
docker rmi ${CAA3_DOCKERHUB_USER}/${IMAGE_NAME}:${VERSION}

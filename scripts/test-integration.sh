#!/bin/bash -e

if [[ -n "${CI_COMMIT_TAG}" ]]; then
  VERSION=$(echo $CI_COMMIT_TAG)
else
  VERSION="0.0.0-SNAPSHOT"
fi
echo "Version: ${VERSION}"

if [[ -n "$CI_PROJECT_DIR" ]]; then
  PROJECT_DIR=$(echo $CI_PROJECT_DIR)
else
  PROJECT_DIR="."
fi
echo "Project Directory: ${PROJECT_DIR}"

sed -i -e "s#caa3/backend:0.0.0-SNAPSHOT#caa3/backend:${VERSION}#g" $PROJECT_DIR/integration-test/docker-compose.yml
cd $PROJECT_DIR/integration-test/
docker-compose up --exit-code-from test-driver

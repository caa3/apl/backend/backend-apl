#!/bin/bash -e

if [[ -n "${CI_COMMIT_TAG}" ]]; then
  VERSION=$(echo $CI_COMMIT_TAG)
else
  VERSION="0.0.0-SNAPSHOT"
fi
echo "Version: ${VERSION}"

yarn lint:report

#!/bin/bash -e

if [[ -n "${CI_COMMIT_TAG}" ]]; then
  VERSION=$(echo $CI_COMMIT_TAG)
else
  VERSION="0.0.0-SNAPSHOT"
fi
echo "Version: ${VERSION}"

if [ ! -n "${CAA3_DOCKERHUB_USER}" ]; then
  STR+="No settings; CAA3_DOCKERHUB_USER of GitLab Variables.\n"
  ERROR_FLG="true"
fi

if [ ! -n "${CAA3_DOCKERHUB_PASSWORD}" ]; then
  STR+="No settings; CAA3_DOCKERHUB_PASSWORD of GitLab Variables.\n"
  ERROR_FLG="true"
fi

if [ "$ERROR_FLG" = "true" ]; then
  echo -e ${STR}
  exit 9
else
  echo "Check OK\n"
fi

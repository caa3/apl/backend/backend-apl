openapi: 3.0.0
info:
  title: "Cafe API"
  version: "1.0.0"
servers:
  - url: "https://lra-app.com:443/api/v1"
    description: |
      production
  - url: "{protocol}://{host}:{port}/api/v1"
    description: |
      development
    variables:
      host:
        default: lra-app.com
        enum:
          - lra-app.com
          - localhost
      protocol:
        default: https
        enum:
          - https
          - http
      port:
        default: "443"
        enum:
          - "443"
          - "80"
security:
  - api_key: []
paths:
  /cafes:
    get:
      tags:
        - cafes
      summary: |
        Search cafes
      description: |
        Search cafes in catalog
        using a free query parameter.
      parameters:
        - name: accept
          description: |
            Accept header
          in: header
          schema:
            type: string
            default: application/json
            enum:
              - application/json
        - name: accept-language
          description: |
            Accept-language header
          in: header
          schema:
            type: string
            default: en-US
            enum:
              - en-US
              - ja-JP

        - name: name
          description: |
            A cafes's name.
          in: query
          required: false
          schema:
            type: string
        - name: latitude
          description: |
            A cafes's latitude.
          in: query
          required: false
          schema:
            type: number
        - name: longitude
          description: |
            A cafes's longitude.
          in: query
          required: false
          schema:
            type: number
        - name: address
          description: |
            A cafes's address.
          in: query
          required: false
          schema:
            type: string
        - name: postalCode
          description: |
            A cafes's postal code.
          in: query
          required: false
          schema:
            type: string
      responses:
        "200":
          description: |
            Cafes matching the query
          content:
            application/json:
              schema:
                type: array
                description: |
                  Array of the cafe
                items:
                  $ref: "#/components/schemas/cafe"
          headers:
            Content-Type:
              description: The content's type
              schema:
                type: string
                example: application/json; charset=utf-8
            Content-Language:
              description: The content's language
              schema:
                type: string
                example: en-US
        "400":
          description: |
            Bad request query
          content:
            application/json:
              schema:
                type: array
                description: |
                  Array of the error
                items:
                  $ref: "#/components/schemas/error"
          headers:
            Content-Type:
              description: The content's type
              schema:
                type: string
                example: application/json; charset=utf-8
            Content-Language:
              description: The content's language
              schema:
                type: string
                example: en-US
        "405":
          description: |
            Bad request http method
          content:
            application/json:
              schema:
                type: array
                description: |
                  Array of the error
                items:
                  $ref: "#/components/schemas/error"
          headers:
            Content-Type:
              description: The content's type
              schema:
                type: string
                example: application/json; charset=utf-8
            Content-Language:
              description: The content's language
              schema:
                type: string
                example: en-US
        "406":
          description: |
            Bad request accept header
          content:
            application/json:
              schema:
                type: array
                description: |
                  Array of the error
                items:
                  $ref: "#/components/schemas/error"
          headers:
            Content-Type:
              description: The content's type
              schema:
                type: string
                example: application/json; charset=utf-8
            Content-Language:
              description: The content's language
              schema:
                type: string
                example: en-US
        "415":
          description: |
            Bad request content type header
          content:
            application/json:
              schema:
                type: array
                description: |
                  Array of the error
                items:
                  $ref: "#/components/schemas/error"
          headers:
            Content-Type:
              description: The content's type
              schema:
                type: string
                example: application/json; charset=utf-8
            Content-Language:
              description: The content's language
              schema:
                type: string
                example: en-US
        "500":
          description: |
            Internal server error
          content:
            application/json:
              schema:
                type: array
                description: |
                  Array of the error
                items:
                  $ref: "#/components/schemas/error"
          headers:
            Content-Type:
              description: The content's type
              schema:
                type: string
                example: application/json; charset=utf-8
            Content-Language:
              description: The content's language
              schema:
                type: string
                example: en-US
components:
  schemas:
    error:
      description: |
        error's schema
      type: object
      required:
        - code
        - message
      properties:
        code:
          description: |
            error's code
          type: string
          example: E001
        message:
          description: |
            message for error's detail information
          type: string
          example: query is nothing
    location:
      description: |
        the cafe's location
      type: object
      required:
        - latitude
        - longitude
        - adress
        - postalCode
      properties:
        latitude:
          description: |
            latitude of the cafe
          type: number
          example: 111.111
        longitude:
          description: |
            longitude of the cafe
          type: number
          example: 111.111
        address:
          description: |
            address of the cafe
          type: string
          example: xxxxx
        postalCode:
          description: |
            postal code of the cafe
          type: string
          example: xxxxx
    cafe: 
      type: object
      description: |
        The cafe
      required:
        - reference
        - name
        - location
      properties:
        reference:
          description: |
            unique ID identifying the cafe
          type: string
          example: xxxxx
        name:
          description: |
            the cafe's name
          type: string
          example: xxxxx
        description:
          description: |
            the cafe's description
          type: string
          example: xxxxx
        location:
          $ref: "#/components/schemas/location"
  securitySchemes:
    api_key:
      type: apiKey
      name: x-api-key
      in: header
tags:
  - name: cafes
    description: |
      Cafes

# pagenation and hypermedia are not implemented.
# validation is not implemented.
# https://swagger.io/specification/

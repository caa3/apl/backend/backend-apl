import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { HttpClientService } from './services/http-client/http-client.service';
import { ConfigService } from './services/config/config.service';

@Module({
  imports: [HttpModule],
  providers: [HttpClientService, ConfigService],
  exports: [HttpClientService, ConfigService],
})
export class CoreModule {}

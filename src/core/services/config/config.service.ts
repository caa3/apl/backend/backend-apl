import { Injectable } from '@nestjs/common';
import * as dotenv from 'dotenv';
import * as fs from 'fs';
import * as path from 'path';
import { EnvConfig } from './interfaces';
// HttpModule
import { HttpModuleOptions } from '@nestjs/axios';
// JwtModule
import { JwtModuleOptions } from '@nestjs/jwt';

@Injectable()
export class ConfigService {
  private readonly envConfig: EnvConfig;

  constructor() {
    const filePath = `.${process.env.NODE_ENV || 'development'}.env`;
    const envFile = path.resolve(__dirname, '../../../../', filePath);
    if (fs.existsSync(envFile)) {
      this.envConfig = dotenv.parse(fs.readFileSync(envFile));
    }
  }

  public get(key: string): string {
    return process.env[key] || this.envConfig[key];
  }

  public getArray(key: string, delimiter = ','): string[] {
    return this.get(key).split(delimiter) || [];
  }

  public get isDevelopment(): boolean {
    return this.get('NODE_ENV') === 'development';
  }

  public get isProduction(): boolean {
    return this.get('NODE_ENV') === 'production';
  }

  public getNumber(key: string): number {
    return Number(this.get(key));
  }

  public get nodeEnv(): string {
    return this.get('NODE_ENV') || 'development';
  }

  public get fallbackLanguage(): string {
    return this.get('FALLBACK_LANGUAGE').toLowerCase();
  }

  get httpModuleOptions(): HttpModuleOptions {
    return {
      timeout: this.getNumber('HTTP_TIMEOUT'),
      maxRedirects: this.getNumber('HTTP_MAX_REDIRECTS'),
    };
  }

  get jwtModuleOptions(): JwtModuleOptions {
    return {
      secret: this.get('JWT_SECRET'),
      signOptions: {
        expiresIn: this.getNumber('JWT_EXPIRES_IN'),
      },
    };
  }
}

import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { AxiosResponse } from 'axios';
import { firstValueFrom } from 'rxjs';
import { DEFAULT_ERROR } from './constants';

@Injectable()
export class HttpClientService {
  constructor(private readonly httpService: HttpService) {}

  /** @param {string} url */
  public getAll<U = any>(url: string): Promise<AxiosResponse<U, any>> {
    return firstValueFrom(this.httpService.get<U>(`${url}`))
      .then((response) => {
        return response;
      })
      .catch((error: any) => {
        if (error instanceof Error) {
          throw error;
        } else {
          throw new Error(DEFAULT_ERROR);
        }
      });
  }

  /** @param {string} url */
  /** @param {object} model */
  public getAllByQuery<T = any, U = any>(
    url: string,
    model: T,
  ): Promise<AxiosResponse<U, any>> {
    return firstValueFrom(this.httpService.get<U>(`${url}`, { params: model }))
      .then((response) => {
        return response;
      })
      .catch((error: any) => {
        if (error instanceof Error) {
          throw error;
        } else {
          throw new Error(DEFAULT_ERROR);
        }
      });
  }

  /** @param {string} url */
  /** @param {string} id */
  public getByKey<U = any>(
    url: string,
    id: string,
  ): Promise<AxiosResponse<U, any>> {
    return firstValueFrom(this.httpService.get<U>(`${url}/${id}`))
      .then((response) => {
        return response;
      })
      .catch((error: any) => {
        if (error instanceof Error) {
          throw error;
        } else {
          throw new Error(DEFAULT_ERROR);
        }
      });
  }

  /** @param {string} url */
  /** @param {object} model */
  public post<T = any, U = any>(
    url: string,
    model: T,
  ): Promise<AxiosResponse<U, any>> {
    return firstValueFrom(this.httpService.post<U>(`${url}`, model))
      .then((response) => {
        return response;
      })
      .catch((error: any) => {
        if (error instanceof Error) {
          throw error;
        } else {
          throw new Error(DEFAULT_ERROR);
        }
      });
  }

  /** @param {string} url */
  /** @param {object} model */
  public put<T = any, U = any>(
    url: string,
    model: T,
  ): Promise<AxiosResponse<U, any>> {
    return firstValueFrom(this.httpService.put<U>(`${url}`, model))
      .then((response) => {
        return response;
      })
      .catch((error: any) => {
        if (error instanceof Error) {
          throw error;
        } else {
          throw new Error(DEFAULT_ERROR);
        }
      });
  }

  /** @param {string} url */
  /** @param {object} model */
  public patch<T = any, U = any>(
    url: string,
    model: T,
  ): Promise<AxiosResponse<U, any>> {
    return firstValueFrom(this.httpService.patch<U>(`${url}`, model))
      .then((response) => {
        return response;
      })
      .catch((error: any) => {
        if (error instanceof Error) {
          throw error;
        } else {
          throw new Error(DEFAULT_ERROR);
        }
      });
  }

  /** @param {string} url */
  /** @param {string} id */
  public remove<U = any>(
    url: string,
    id: string,
  ): Promise<AxiosResponse<U, any>> {
    return firstValueFrom(this.httpService.delete<U>(`${url}/${id} `))
      .then((response) => {
        return response;
      })
      .catch((error: any) => {
        if (error instanceof Error) {
          throw error;
        } else {
          throw new Error(DEFAULT_ERROR);
        }
      });
  }
}

import { Controller, Query, Get } from '@nestjs/common';
import { GetCafesInDto, GetCafesOutDto } from './dto/';
import { CafesService } from './cafes.service';

@Controller('cafes')
export class CafesController {
  constructor(private cafesService: CafesService) {}

  /**
   * REST: GET /cafes
   */
  @Get()
  public async getCafesByKeys(
    @Query() inDto: GetCafesInDto,
  ): Promise<GetCafesOutDto> {
    return this.cafesService.getCafesByKeys(inDto);
  }
}

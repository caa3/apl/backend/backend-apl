interface IGetCafesInDto {
  name?: string;
  latitude?: number;
  longitude?: number;
  address?: string;
  postalCode?: string;
  query?: string;
}

export class GetCafesInDto implements IGetCafesInDto {
  public name?: string;
  public latitude?: number;
  public longitude?: number;
  public address?: string;
  public postalCode?: string;
  public query?: string;

  constructor(props?: IGetCafesInDto) {
    this.name = props?.name;
    this.latitude = props?.latitude;
    this.longitude = props?.longitude;
    this.address = props?.address;
    this.postalCode = props?.postalCode;
    this.query = props?.query;
  }

  public create(props?: IGetCafesInDto): GetCafesInDto {
    return new GetCafesInDto(props);
  }
}

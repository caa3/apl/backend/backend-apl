interface IGetCafesOutDto {
  cafes?: Array<Cafe>;
}

export class GetCafesOutDto implements IGetCafesOutDto {
  public cafes: Array<Cafe>;

  constructor(props?: IGetCafesOutDto) {
    this.cafes = props?.cafes ? props.cafes : [];
  }

  public create(props?: IGetCafesOutDto): GetCafesOutDto {
    return new GetCafesOutDto(props);
  }
}

interface ICafe {
  reference?: string;
  name?: string;
  description?: string;
  location?: Location;
}

export class Cafe implements ICafe {
  public reference?: string;
  public name?: string;
  public description?: string;
  public location?: Location;

  constructor(props?: ICafe) {
    this.reference = props?.reference;
    this.name = props?.name;
    this.description = props?.description;
    this.location = props?.location;
  }

  public create(props?: ICafe): Cafe {
    return new Cafe(props);
  }
}

interface ILocation {
  latitude?: number;
  longitude?: number;
  address?: string;
  postalCode?: string;
}

export class Location implements ILocation {
  public latitude?: number;
  public longitude?: number;
  public address?: string;
  public postalCode?: string;

  constructor(props?: ILocation) {
    this.latitude = props?.latitude;
    this.longitude = props?.longitude;
    this.address = props?.address;
    this.postalCode = props?.postalCode;
  }

  public create(props?: ILocation): Location {
    return new Location(props);
  }
}

import { Injectable } from '@nestjs/common';
import { GetCafesInDto, GetCafesOutDto, Cafe, Location } from './dto/';

import { HttpClientService } from 'src/core/services/http-client/http-client.service';
import { ConfigService } from 'src/core/services/config/config.service';
import {
  URL_GOOGLE_MAPS_PLACES_TEXT,
  ACCESS_KEY_GOOGLE,
} from 'src/constants/env.constant';
import { GoogleMapsPlacesTextRequestType } from './types/google-maps-places-text-request.type';
import { GoogleMapsPlacesTextResponseType } from './types/google-maps-places-text-response.type';

@Injectable()
export class CafesService {
  constructor(
    private readonly httpClientService: HttpClientService,
    private readonly configService: ConfigService,
  ) {}

  /**
   * REST: GET /cafes
   */
  public async getCafesByKeys(inDto: GetCafesInDto): Promise<GetCafesOutDto> {
    const url = this.configService.get(URL_GOOGLE_MAPS_PLACES_TEXT);
    const key = this.configService.get(ACCESS_KEY_GOOGLE);
    const language = 'ja';
    const query = inDto.query;
    if (typeof query === 'undefined') {
      return new GetCafesOutDto();
    }
    const request: GoogleMapsPlacesTextRequestType = {
      key,
      language,
      query,
    };
    const response = await this.httpClientService.getAllByQuery<
      GoogleMapsPlacesTextRequestType,
      GoogleMapsPlacesTextResponseType
    >(url, request);
    // console.log(JSON.stringify(request));
    // console.log(url);
    // console.log(key);
    // console.log(JSON.stringify(response.data));
    // console.log(response.status);
    // console.log(JSON.stringify(response.data));
    // console.log(JSON.stringify(request));
    const cafes = new Array<Cafe>();
    for (const [, result] of response.data.results.entries()) {
      if (result === null || undefined) {
        break;
      }
      const name = result.name;
      const reference = result.reference;
      // const description = '';
      const latitude = result.geometry.location.lat;
      const longitude = result.geometry.location.lng;
      // const address = '';
      // const postalCode = '';
      const location = new Location({ latitude, longitude });
      const cafe = new Cafe({ name, reference, location });
      cafes.push(cafe);
    }
    return new GetCafesOutDto({ cafes });
  }
}

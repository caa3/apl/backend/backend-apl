export type GoogleMapsPlacesTextResponseType = {
  html_attributions: Array<any>;
  results: Array<Result>;
};

export type Result = {
  business_status: string;
  formatted_address: string;
  geometry: Geometry;
  icon: string;
  icon_background_color: string;
  icon_mask_base_uri: string;
  name: string;
  opening_hours: OpeningHours;
  photos: Array<Photo>;
  place_id: string;
  plus_code: PlusCode;
  price_level: number;
  rating: number;
  reference: string;
  types: Array<string>;
  user_ratings_total: number;
};

export type Geometry = {
  location: Location;
};

export type Location = {
  lat: number;
  lng: number;
};

export type Viewport = {
  northeast: Location;
  southwest: Location;
};

export type Photo = {
  height: number;
  width: number;
  html_attributions: Array<string>;
  photo_reference: string;
};

export type OpeningHours = {
  open_now: boolean;
};

export type PlusCode = {
  compound_code: string;
  global_code: string;
};

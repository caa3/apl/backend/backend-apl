export type GoogleMapsPlacesTextRequestType = {
  key: string;
  query: string;
  language: string;
};

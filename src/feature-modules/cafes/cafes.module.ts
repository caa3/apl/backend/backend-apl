import { Module } from '@nestjs/common';
import { CoreModule } from 'src/core/core.module';
import { CafesController } from './cafes.controller';
import { CafesService } from './cafes.service';

@Module({
  imports: [CoreModule],
  controllers: [CafesController],
  providers: [CafesService],
})
export class CafesModule {}

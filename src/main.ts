import { NestFactory, Reflector } from '@nestjs/core';
import { AppModule } from './app.module';
import {
  NestExpressApplication,
  ExpressAdapter,
} from '@nestjs/platform-express';
import { ClassSerializerInterceptor, ValidationPipe } from '@nestjs/common';
import helmet from 'helmet';
import * as compression from 'compression';
import * as cookieParser from 'cookie-parser';
import {
  initializeTransactionalContext,
  patchTypeORMRepositoryWithBaseRepository,
} from 'typeorm-transactional-cls-hooked';

async function bootstrap() {
  /**
   * Transaction Settings.
   */
  initializeTransactionalContext();
  patchTypeORMRepositoryWithBaseRepository();

  /**
   * Initialize Settings For App.
   */
  const app = await NestFactory.create<NestExpressApplication>(
    AppModule,
    new ExpressAdapter(),
    { cors: true },
  );

  /**
   * Global Middleware For Http.
   */
  app.enable('trust proxy');
  app.setGlobalPrefix('v1');
  app.use(helmet());
  app.use(compression());
  app.use(cookieParser());

  /**
   * Global Interceptors For Http.
   */
  const reflector = app.get(Reflector);
  app.useGlobalInterceptors(new ClassSerializerInterceptor(reflector));
  app.useGlobalPipes(new ValidationPipe());

  /**
   * Start Http Server.
   */
  await app.listen(3000);
}
bootstrap();

import { Module } from '@nestjs/common';
import { CafesModule } from './feature-modules/cafes/cafes.module';
import { CoreModule } from './core/core.module';

@Module({
  imports: [CafesModule, CoreModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
